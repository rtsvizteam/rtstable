<!-- README.md is generated from README.Rmd. Please edit that file --> 

rtstable
====


Plot Table With Base R Graphics
===


Plot Table with base R Graphics.



Installation:
===


To install the development version run following code:


```r
remotes::install_bitbucket("rtsvizteam/rtstable")
```
	

Example :
===


```r
	# load `rtstable` package
	library(rtstable)

	
    library(rtstable)

    # test data
    temp = matrix(1:12,3,4)
        rownames(temp) = c('row one','row two','row 3')
        colnames(temp) = c('col 1','col 2','col 3','col 4')		

    # setup
    bd = rtstable.border()
    ft = rtstable.font()
    ad = rtstable.adj()

    # plot table with header, borders, and alternating rows
    rtstable(temp, format(as.Date(Sys.time()), '%d %b %Y'), same.size=TRUE,
        par.title = rtstable.param(border = bd$all, adj = ad$left, bgcol=c('gray'), font=ft$bold),
        par.col = rtstable.param(border = bd$all, adj = ad$left, bgcol=c('gray'), font=ft$bold),
        par.row = rtstable.param(border = bd$all, adj = ad$right, bgcol=c('white','yellow'), font=ft$bold),
        par.body = rtstable.param(border = bd$all, adj = ad$center, bgcol=c('white','yellow'))		
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-1.png)

```r
    # plot table with with dashed borders
    rtstable(temp, same.size=TRUE,
        par.col = rtstable.param(border = bd$left+bd$right, blwd=5, blty=2, bcol='red', adj = ad$left),
        par.body = rtstable.param(border = bd$left+bd$right, blwd=5, blty=2, bcol='red', adj = ad$center),
        row.names=FALSE
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-2.png)

```r
    # plot table with basic styling
    rtstable(temp, same.size=TRUE,
        par.title = rtstable.param(),
        par.col = rtstable.param(),    
        par.row = rtstable.param(adj = ad$right, font=ft$italic),
        par.body = rtstable.param(bgcol=c('white',grDevices::adjustcolor('gray', 100/255)), adj = ad$center)		
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-3.png)

```r
    # plot table with highlighting
    rtstable(temp, same.size=TRUE,
        par.title = rtstable.param(),
        par.col = rtstable.param(),
        par.row = rtstable.param(),	
        par.body = rtstable.param(border = bd$all, adj = ad$center),
        highlight = TRUE		
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-4.png)

```r
    # plot table with highlighting set externally
    highlight = rtstable.highlight(temp)
    rtstable(temp, same.size=TRUE,
        par.title = rtstable.param(),
        par.col = rtstable.param(),
        par.row = rtstable.param(),	
        par.body = rtstable.param(border = bd$all, adj = ad$center, bgcol=highlight)		
    )

    
    # plot table with highlighting and colorbar
    rtstable(temp, same.size=TRUE,
        par.title = rtstable.param(),
        par.col = rtstable.param(),
        par.row = rtstable.param(),	
        par.body = rtstable.param(border = bd$all, adj = ad$center),
        highlight = TRUE,
        colorbar = TRUE		
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-5.png)

```r
    # plot table with custom highlighting
    col = grDevices::rainbow(ncol(temp))
    highlight = apply(temp,2, function(x) col[order(x, decreasing = TRUE)] )

    rtstable(temp, same.size=TRUE,
        par.title = rtstable.param(),
        par.col = rtstable.param(),
        par.row = rtstable.param(),	
        par.body = rtstable.param(border = bd$all, adj = ad$center, bgcol=highlight)		
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-6.png)

```r
    # plot correlation table with highlighting
    n = 10
    data =  matrix(stats::rnorm(1000), ncol=n)
        colnames(data) = paste('data', 1:n, sep='')

    data = temp = stats::cor(data, use='complete.obs', method='pearson')
        temp[] = paste0(round(100 * temp, 0), '%')

    # plot table
    rtstable(temp, 'Correlation', same.size=FALSE,
        par.body = rtstable.param(border = bd$all, adj = ad$center)
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-7.png)

```r
    # plot temp with colorbar
    rtstable(temp, , highlight = TRUE, colorbar = TRUE)	
```

![plot of chunk plot-2](/man/figures/README/plot-2-8.png)

```r
    # plot temp with colorbar and fromatting
    rtstable(temp, 'Correlation', same.size=FALSE,        
        par.title = rtstable.param(),
        par.col = rtstable.param(),
        par.row = rtstable.param(),	
        par.body = rtstable.param(border = bd$all, adj = ad$center),
        highlight = TRUE,
        colorbar = TRUE		
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-9.png)

```r
    # highlight each column separately 		
    highlight = apply(temp,2, function(x) rtstable.highlight(t(x)) )

    rtstable(temp, same.size=TRUE,
        par.title = rtstable.param(),
        par.col = rtstable.param(),
        par.row = rtstable.param(),	
        par.body = rtstable.param(border = bd$all, adj = ad$center, bgcol=highlight),
        colorbar = TRUE
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-10.png)

```r
    # Construct Periodic table, like in Single Country Index Returns
    # http://us.ishares.com/content/stream.jsp?url=/content/en_us/repository/resource/single_country_periodic_table.pdf&mimeType=application/pdf            
    temp = data
    rownames(temp) = 1:n
        rownames(temp)[1] = ' Best '
        rownames(temp)[n] = ' Worst '

    # highlight each column
    col = rainbow(n, start = 0, end = 0.3)            
    highlight = apply(temp,2, function(x) col[order(x, decreasing = TRUE)] )
    temp[] = apply(temp,2, sort, decreasing = TRUE)
    
    # format data as percentages
    temp[] = paste0(round(100 * temp, 0), '%')

    rtstable(temp, same.size=TRUE,
        par.title = rtstable.param(),
        par.col = rtstable.param(),
        par.row = rtstable.param(),	
        par.body = rtstable.param(border = bd$all, adj = ad$center, bgcol=highlight),
        colorbar = TRUE
    )
```

![plot of chunk plot-2](/man/figures/README/plot-2-11.png)
