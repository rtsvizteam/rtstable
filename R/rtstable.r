#' @title `rtstable` - Plot Table with base R Graphics.
#'
#' @description Plot Table with base R Graphics.
#'
#' The `rtstable` package is **fast** package to plot table with base R Graphics.
#'
#' There are 4 parts to the table: title(1), columns(2), rows(3), body(4)
#'
#' |.1.|......3..........|
#' |---|-----------------|
#' |...|.................|
#' |.2.|......4..........|
#' |...|.................|
#'
#' For each section specify following parameters:
#' * adj = left / mid / right
#' * col
#' * bg.col
#' * font
#' * border, lty, lwd
#'
#' By default single entries for each section will be mapped to each element.
#' You can specify alternating entries without providing full matrix of parameters.
#' For example, in section (4) to alternate background row colors, set
#' bg.col = c('white', 'yellow')
#'
#' @examples
#'	# test data
#'	temp = matrix(1:12,3,4)
#'		rownames(temp) = c('row one','row two','row 3')
#'		colnames(temp) = c('col 1','col 2','col 3','col 4')		
#'		
#'	# setup
#'	bd = rtstable.border()
#'	ft = rtstable.font()
#'	ad = rtstable.adj()
#'	
#'	# plot	
#'	rtstable(temp, same.size=TRUE,
#'		par.title = rtstable.param(border = bd$all, adj = ad$left, bgcol=c('gray'), font=ft$bold),
#'		par.col = rtstable.param(border = bd$all, adj = ad$left, bgcol=c('gray'), font=ft$bold),
#'		par.row = rtstable.param(border = bd$all, adj = ad$right, bgcol=c('white','yellow'), font=ft$bold),
#'		par.body = rtstable.param(border = bd$all, adj = ad$center, bgcol=c('white','yellow'))		
#'	)
#'	
#'
#' @importFrom graphics lines par plot rect strheight strwidth text
#' @importFrom grDevices dev.off png rainbow
#'
#'
#' @name rtstable
#' @docType package
#'
NULL






#------------------------------------------------------------------------------
# make.table - create empty plot, based on the example at
# http://addictedtor.free.fr/graphiques/RGraphGallery.php?graph=28
#------------------------------------------------------------------------------
rtstable.make.table = function
(
    nr,	# number of rows
    nc	# number of columns
)
{
    savepar = par(mar = rep(1, 4))
    plot(c(0.5, nc*2 + 0.5), c(-0.5, -(nr + 0.5)),
        xaxs = 'i', yaxs = 'i', type = 'n', xlab = '', ylab = '', axes = FALSE)
    savepar
}


#------------------------------------------------------------------------------
# draw.cell - draw cell at location (r,c)
#------------------------------------------------------------------------------
rtstable.draw.cell = function
(
    title,				# text to draw in this cell
    r,					# row
    c,					# column

    cex = 1,			# size of text
    font = 1,			# font type
    tcol = 'black',		# text color
    adj = 1,			# adjusment

    bgcol = 'white',	# background color

    border = 0,			# border type
    bcol = 'black',		# border color
    blwd = 1,			# border line width
    blty = 1			# border line type
){
    x0 = (2*(c - 1) + .5)
    y0 = -(r + .5)

    x1 = (2*c + .5)
    y1 = -(r - .5)


    # lines take a while to plot
    fast.border = FALSE
    if(border > 0 && rtstable.border.map[border] == 5) #all
    if(blty == 1 | blwd == 1)
        fast.border = TRUE

    # background
    if(fast.border) {
        rect(x0, y0, x1, y1, col = bgcol, border = bcol, lwd = blwd, lty = blty)
        border = 0
    } else
        if(!is.na(bgcol)) rect(x0, y0, x1, y1, col = bgcol, border = NA)



    # border = function() list(top=1, bot=10, left=100, right=1000)
    if(border > 0) {	
    # border
    lines1 = function(x,y) lines(x, y, lwd = blwd, lty = blty, col = bcol)
    switch(rtstable.border.map[border]
        ,lines1(c(x0,x1), c(y1,y1)) # top 1
        ,lines1(c(x0,x1), c(y0,y0)) # bot 2
        ,lines1(c(x0,x0), c(y0,y1)) # left 3
        ,lines1(c(x1,x1), c(y0,y1)) # right 4		
        #,lines1(c(x0,x1,x1,x0,x0), c(y1,y1,y0,y0,y1)) # top,bot,left,right 5		
        ,{lines1(c(x0,x1), c(y1,y1));lines1(c(x0,x1), c(y0,y0));lines1(c(x0,x0), c(y0,y1));lines1(c(x1,x1), c(y0,y1))} # top,bot,left,right 5				
        ,{lines1(c(x0,x1), c(y1,y1));lines1(c(x0,x1), c(y0,y0))} # top,bot 6
        ,{lines1(c(x0,x1), c(y1,y1));lines1(c(x0,x0), c(y0,y1))} # top,left 7
        ,{lines1(c(x0,x1), c(y1,y1));lines1(c(x1,x1), c(y0,y1))} # top,right 8
        ,{lines1(c(x0,x1), c(y0,y0));lines1(c(x0,x0), c(y0,y1))} # bot,left 9
        ,{lines1(c(x0,x1), c(y0,y0));lines1(c(x1,x1), c(y0,y1))} # bot,right 10
        ,{lines1(c(x0,x0), c(y0,y1));lines1(c(x1,x1), c(y0,y1))} # left,right 11		
        ,{lines1(c(x0,x1), c(y1,y1));lines1(c(x0,x1), c(y0,y0));lines1(c(x0,x0), c(y0,y1))} # top,bot,left 12
        ,{lines1(c(x0,x1), c(y1,y1));lines1(c(x0,x1), c(y0,y0));lines1(c(x1,x1), c(y0,y1))} # top,bot,right 13
        ,{lines1(c(x0,x1), c(y1,y1));lines1(c(x0,x0), c(y0,y1));lines1(c(x1,x1), c(y0,y1))} # top,left,right 14
        ,{lines1(c(x0,x1), c(y0,y0));lines1(c(x0,x0), c(y0,y1));lines1(c(x1,x1), c(y0,y1))} # bot,left,right 15
    )
    }	

    # text
    # [Integer specifying font to use for text. 1=plain, 2=bold, 3=italic, 4=bold italic, 5=symbol](https://www.statmethods.net/advgraphs/parameters.html)
    # plot(0:6, 0:6, type="n"); text(1:5, 1:5, paste("font = ", 1:5), font=1:5, cex=3)	
    # adj = function() list(left=1, right=2, middle=3)
    if(adj == 1) {# left adjusted
            text((2*(c - 1) + .5), -r, 	title, adj = 0,cex = cex, font = font, col = tcol)
        }
    else if(adj == 2)
        {# right adjusted
            text((2*c + .5), -r, 		title, adj = 1,cex = cex, font = font, col = tcol)
        }
    else if(adj == 3)		
        {# center adjusted
            text((2*c - .5), -r, 		title, adj = 0.5,cex = cex, font = font, col = tcol)
        }



}


###############################################################################
# draw.cell - draw cell at location (r,c)
###############################################################################
draw.cell <- function
(
    title,				# text to draw in this cell
    r,					# row
    c,					# column
    text.cex = 1,		# size of text
    bg.col = 'white',	# background color
    frame.cell = TRUE		# flag to draw border around this cell
)
{
    if(!frame.cell) bcol = bg.col else bcol = 'black'
    rect((2*(c - 1) + .5), -(r - .5), (2*c + .5), -(r + .5), col = bg.col, border = bcol)

    if( c == 1) { # first column
        text((2*(c - 1) + .5), -r, title, adj = 0, cex = text.cex)
    } else if( r == 1 ) { # first row
        text((2*(c - 1) + .5), -r, title, adj = 0, cex = text.cex)
    } else {
        text((2*c + .5), -r, title, adj = 1, cex = text.cex)
    }
}


#------------------------------------------------------------------------------
# border map
#------------------------------------------------------------------------------
# 1	 top		1
# 2	 bot		10
# 3	 left		100
# 4	 right		1000
# 5	 top,bot,left,right	1111
# 6	 top,bot		11
# 7	 top,left		101
# 8	 top,right		1001
# 9	 bot,left		110
# 10 bot,right		1010
# 11 left,right		1100
# 12 top,bot,left	111
# 13 top,bot,right	1011
# 14 top,left,right	1101
# 15 bot,left,right	1110
# border = function() list(top=1, bot=10, left=100, right=1000)
# should be global
rtstable.border.map = 1:1111
rtstable.border.map[c(1,10,100,1000,1111,11,101,1001,110,1010,1100,111,1011,1101,1110)]=1:15



#------------------------------------------------------------------------------
# determine how to auto-adjust text size
#  code is based on discussion at
#  http://www.mail-archive.com/r-help@r-project.org/msg04577.html			
#------------------------------------------------------------------------------
rtstable.auto.cex = function
(
    mtxt, 				# matrix to plot
    same.size = FALSE	# flag to auto-adjust text size
)
{
    nr = nrow(mtxt)
    nc = ncol(mtxt)

    xrange = matrix( strwidth(paste('  ', mtxt), units = 'user', cex = 1), ncol = nc)
    yrange = matrix( 5/3 * strheight(mtxt, units = 'user', cex = 1), ncol = nc)

    mtxt.cex = round(pmin(
        diff(par()$usr[3:4]) / nr / yrange,
        diff(par()$usr[1:2]) / nc / xrange
    ), 2)

    if ( same.size ) {
        mtxt.cex[] = min(mtxt.cex)
    } else {		
        header.col.cex = min(mtxt.cex[1,-1])
        header.row.cex = min(mtxt.cex[-1,1])
        title.cex = mtxt.cex[1, 1]
        data.cex = min(mtxt.cex[-1, -1])	

        mtxt.cex[1,-1] = header.col.cex
        mtxt.cex[-1,1] = header.row.cex		
        mtxt.cex[-1,-1]= data.cex		
        mtxt.cex[1,1]= title.cex
    }
    mtxt.cex
}





###############################################################################
#' Highlight colors
#'
#' Generate colors to highlight data in table
#'
#' @param temp data matrix
#'
#' @return matrix with highlight colors
#'
#' @export
###############################################################################
rtstable.highlight = function(temp)
{
    # convert temp to numerical matrix
    temp = matrix(as.double(gsub('[%,$]', '', temp)), nrow(temp), ncol(temp))

    highlight = as.vector(temp)
    cols = rep(NA, len(highlight))
        ncols = len(highlight[!is.na(highlight)])
        cols[1:ncols] = rainbow(ncols, start = 0, end = 0.3)			

    o = sort.list(highlight, na.last = TRUE, decreasing = FALSE)
        o1 = sort.list(o, na.last = TRUE, decreasing = FALSE)
        highlight = matrix(cols[o1], nrow = nrow(temp))
        highlight[is.na(temp)] = NA
    highlight
}


###############################################################################
#' Draw Color Bar
#'
#' Generate colors bar beside the table
#'
#' @param temp data matrix
#'
#' @return nothing
#'
#' @export
###############################################################################
rtstable.colorbar = function(temp)
{
    nr = nrow(temp) + 1
    nc = ncol(temp) + 2

    c = nc
    r1 = 1
    r2 = nr

    x0 = (2*(c - 1) + .5)
    y0 = -(r2 + .5)

    x1 = (2*c + .5)
    y1 = -(r1 - .5)

    #rect(x0, y1, x1, y0, col='white', border='white')
       #rect(x0, y1, x0, y0, col='black', border='black')

    y1= c( -(r2) : -(r1) )

    #graphics::image(x = c(  (2*(c - 1) + 1.5) : (2*c + 0.5) ),
    graphics::image(x = c(  (2*(c - 1) + 1.0) : (2*c + 0.0) ),
        y   = y1,
        z   = t(matrix(  y1  , ncol = 1)),
        col = t(matrix( rainbow(len( y1  ), start = 0, end = 0.3) , ncol = 1)),
        add = TRUE)
}



###############################################################################
#' Mappings
#'
#' Mappings for font, border, and adjustment
#'
#' @return list with mappings:
#' font: plain=1, bold=2, italic=3, bold.italic=4, symbol=5
#' adj: left=1, right=2, center=3
#' border: top=1, bot=10, left=100, right=1000, all=1111
#'
#' @export
#' @rdname Mappings
###############################################################################
rtstable.font = function() list(plain=1, bold=2, italic=3, bold.italic=4, symbol=5)

#' @export
#' @rdname Mappings
rtstable.adj = function() list(left=1, right=2, center=3)

#' @export
#' @rdname Mappings
rtstable.border = function() list(top=1, bot=10, left=100, right=1000, all=1111)


###############################################################################
#' Parameters
#'
#' Table settings
#'
#' @param font font type, \strong{defaults to 1 - plain}
#' @param tcol text color, \strong{defaults to 'black'}
#' @param adj adjustment, \strong{defaults to 1 - left}
#' @param bgcol background color, \strong{defaults to 'white'}
#' @param border border type, \strong{defaults to 0 - no border}
#' @param bcol border color, \strong{defaults to 'black'}
#' @param blwd border line width, \strong{defaults to 1}
#' @param blty border line type, \strong{defaults to 1 - solid line}
#'
#' @return list with table parameters
#'
#' @export
###############################################################################
rtstable.param = function(
    font = 1,			# font type
    tcol = 'black',		# text color
    adj = 1,			# text adjustment left/right/center

    bgcol = 'white',	# background color

    border = 0,			# border type
    bcol = 'black',		# border color
    blwd = 1,			# border line width
    blty = 1			# border line type
)
    list(
        font = font,
        tcol = tcol,
        adj = adj,
        bgcol = bgcol,
        border = border,
        bcol = bcol,
        blwd = blwd,
        blty = blty		
    )


###############################################################################
#' Plot table
#'
#' Plot table with user specified parameters
#'
#' @param data data matrix
#' @param title text to draw in top,left cell, \strong{defaults to ''}
#' @param par.body parameters to plot main matrix, \strong{defaults to rtstable.param()}
#' @param par.title parameters to plot title, \strong{defaults to par.body}
#' @param par.row parameters to plot row names, \strong{defaults to par.body}
#' @param par.col parameters to plot col names, \strong{defaults to par.body}
#' @param same.size flag to auto-adjust text size, \strong{defaults to FALSE}
#' @param row.names flag to plot row names, \strong{defaults to TRUE}
#' @param col.names flag to plot column names, \strong{defaults to TRUE}
#' @param highlight flag to highlight with background colors, \strong{defaults to FALSE, highlighting is done with rtstable.highlight()}
#' @param colorbar flag to draw color bar, \strong{defaults to FALSE, color bar is done with rtstable.colorbar()}
#' @param rows.per.page max number of rows per page, \strong{defaults to 120}
#'
#' @return nothing
#'
#' @export
###############################################################################
rtstable = function
(
    data, 				# matrix to plot
    title = '', 		# text to draw in top,left cell

    # define plot parameters
    par.body = rtstable.param(),
    par.title = par.body,
    par.row = par.body,
    par.col = par.body,

    same.size = FALSE,	# flag to auto-adjust text size

    row.names = TRUE,	# flag to plot row names
    col.names = TRUE,	# flag to plot col names

    highlight = FALSE, 	# flag to highlight with background colors
    colorbar = FALSE, 	# flag to draw colorbar		

    rows.per.page = 120	# max number of rows per page
)
{	
    # check if data has dimensions; otherwise convert vector to one column matrix
    if(is.null(dim(data))) dim(data) = c(length(data), 1)

    # split table into multiple pages if too many rows
    n = nrow(data)
    pages = unique(c(seq(0, n, by = rows.per.page), n))


    # parameters
    map.par = function(p, data) {
        n = length(data)
        for(i in names(p)) {
            temp = p[[i]]
            if(length(temp) != n)
                p[[i]] = rep(temp, n)[1:n]
        }
        p
    }

    map.par2 = function(p, data) {
        nr = nrow(data)
        nc = ncol(data)
        for(i in names(p)) {
            temp = p[[i]]
            if(is.matrix(temp)) {
                if(nrow(temp) != nr | ncol(temp) != nc)
                    p[[i]] = matrix(rep(temp, nr)[1:nr], nr,nc)
            } else
                p[[i]] = matrix(rep(temp, nr)[1:nr], nr,nc)

        }
        p
    }

    # copy by value
    for(i in names(par.title)) {
        par.title[[i]] = par.title[[i]]
        par.row[[i]] = par.row[[i]]
        par.col[[i]] = par.col[[i]]
    }
    
    
    par.body = map.par2(par.body, data)	
    if(highlight) par.body$bgcol = rtstable.highlight(data)
    
    # data
    add.space = function(x) paste0(' ',x,' ')
    ncol = 0
    col = colnames(data)
    if(col.names)
        if(!is.null(col)) {
            ncol = 1
            col = add.space(col)
            par.col = map.par(par.col, col)
        }
    nrow = 0	
    row = rownames(data)
    if(row.names)
        if(!is.null(row)) {
            nrow = 1
            row = add.space(row)
            par.row = map.par(par.row, row)
        }
    body = apply(data,2,add.space)
    title = add.space(title)


    # data that fits on one page
    temp.data = function(rindex) {
        temp = body[rindex,,drop=FALSE]

        # append colnames and rownames
        if(ncol) temp = rbind(col, temp)
        if(nrow) temp = cbind(c(if(ncol) title else NULL,row), temp)
        temp
    }	


    nc = ncol(data) + if(nrow > 0) 1 else 0

    for(p in 1:(len(pages)-1)) {	
        rindex = (pages[p]+1) : pages[p+1]

        temp = temp.data(rindex)
            nr = nrow(temp)

        par(mar = c(0, 0, 0, 0), cex = 0.5)

        # add space to the right if colorbar will be drawn
        if(colorbar)
            rtstable.make.table(nr, nc+1)
        else
            rtstable.make.table(nr, nc)		

        cex = rtstable.auto.cex(temp, same.size)

        sr = 1; sc = 1
        if(ncol & nrow) { # title
            r = 1; c = 1; p = par.title
            rtstable.draw.cell(temp[r,c], r, c,
                    cex[r,c], p$font[1], p$tcol[1], p$adj[1],
                    p$bgcol[1],
                    p$border[1], p$bcol[1], p$blwd[1], p$blty[1]
                )
            sr = 2; sc = 2
        }

        if(ncol) { # columns
            r = 1; ; p = par.col
            for(c in sc:nc) {
                i = c - sc + 1
                rtstable.draw.cell(temp[r,c], r, c,
                    cex[r,c], p$font[i], p$tcol[i], p$adj[i],
                    p$bgcol[i],
                    p$border[i], p$bcol[i], p$blwd[i], p$blty[i]
                )
            }
            sr = 2
            rindex = c(0, rindex)
        }

        if(nrow) { # rows
            c = 1; ; p = par.row
            for(r in sr:nr) {
                i = rindex[r]
                rtstable.draw.cell(temp[r,c], r, c,
                    cex[r,c], p$font[i], p$tcol[i], p$adj[i],
                    p$bgcol[i],
                    p$border[i], p$bcol[i], p$blwd[i], p$blty[i]
                )
            }
            sc = 2
        }

        p = par.body
        for(r in sr:nr) { # body
            for(c in sc:nc) {		
                r0 = rindex[r]; i = c - sc + 1
                rtstable.draw.cell(temp[r,c], r, c,
                    cex[r,c], p$font[r0,i], p$tcol[r0,i], p$adj[r0,i],
                    p$bgcol[r0,i],
                    p$border[r0,i], p$bcol[r0,i], p$blwd[r0,i], p$blty[r0,i]
                )
            }
        }

    }
    
    if(colorbar) rtstable.colorbar(data)	

}





